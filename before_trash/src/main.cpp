#include <gtkmm.h>


class MyWindow : public Gtk::Window
{
public:
  MyWindow();
};

MyWindow::MyWindow()
{
  set_title("Basic application");
  set_default_size(200, 200);
}

int main(int argc, char *argv[])
{ 

    int a;
    a++;
  auto app =
    Gtk::Application::create(argc, argv,
      "org.gtkmm.examples.base");

  MyWindow window;

  return app->run(window);
}