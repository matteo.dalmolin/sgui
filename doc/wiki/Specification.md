# Specification

Contributing to the GEN2 Project.

Go back to [README](../../README.md).

---

- [Specification](#specification)
  - [Tool stack](#tool-stack)
  - [Features \[TODO\]](#features-todo)
    - [Manage multiple data streams coming from COMs, TCP/IP, Bluetooth](#manage-multiple-data-streams-coming-from-coms-tcpip-bluetooth)
      - [Protocol specification](#protocol-specification)
    - [Show real time data coming from multiple data sources](#show-real-time-data-coming-from-multiple-data-sources)

---
## Tool stack
-[stack](Stack.md)

## Features [TODO]

- Manage multiple data streams coming from COMs, TCP/IP, Bluetooth;
- Show real time data coming from multiple data sources;
- Have the possibility to save and load the stream data into a binary file;
- Have the possibility to export the stream data into json or csv file;
- Aggregate data streams from multiple sources into one graph;

### Manage multiple data streams coming from COMs, TCP/IP, Bluetooth

- COM library :<https://github.com/wjwwood/serial> , <https://github.com/imabot2/serialib>
- TCP/IP : to be defined <https://github.com/zaphoyd/websocketpp>
- Bluetooth: to be defined

- Protocol for data stream managed as a library, ready to be used by embedded systems;

#### Protocol specification

List of commands:

- How many channels?
- What s the sample rate/timestamp/baudrate?
- Could you start the stream?
- Could you stop the stream?

### Show real time data coming from multiple data sources

- electron + react ??  comunicate across Cpp and nodejs? HOW? dll (windows), so (linux)

react --> website
electron --> web server(react) nodejs with chromium
