# Tools stack

Go back to [README](../../README.md).

---

- [Tools stack](#tools-stack)
  - [Backend \[TODO\]](#backend-todo)
  - [Front End](#front-end)

---

## Backend [TODO]

- nodejs / python as orchwstrator;
- Cpp for high performance data management
- cmake as build system
- ninja as generator

## Front End

- Electron (Windows, Linux, Mac)

---
