# Introduction

The sGui project is composed of these repositories:

## Getting Started

Please read the following documents:

1. [Project specification](doc/wiki/Specification.md)
2. [Software Dependencies](doc/wiki/Dependencies.md)
3. Latest releases

## Build and Test

Please read the following documents:

1. [Building Info](doc/wiki/HowToBuild.md)
2. [Testing Info](doc/wiki/Testing.md)

## Contribute

Please read the following documents:

1. [How to Contribute](doc/wiki/Contribute.md)
